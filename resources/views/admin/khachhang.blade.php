@extends('admin/master')
@section('contentcp')
@if(!Auth::check())
  <script>window.location = "admin/login";</script>
@endif
    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
        <h2>Danh sách khách hàng</h2>
      </div>
      <table id="sanpham" class="display table table-bordered table-hover">
       <thead>
          <tr>
             <th>ID</th>
             <th>Tên KH</th>
             <th>Giới tính</th>
             <th>Email</th>
             <th>Địa chỉ</th>
             <th>Số điện thoại</th>
          </tr>
       </thead>
       <tbody>

        @foreach($customers as $customer)
          <tr>
            <th scope="row">{{ $customer->id }}</th>
             <td>{{ $customer->name }}</td>
             <td>{{ $customer->gender }}</td>
             <td>{{ $customer->email }}</td>
             <td>{{ $customer->address }}</td>
             <td>{{ $customer->phone_number }}</td>
          </tr>
          @endforeach

       </tbody>
      </table>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="sourceadmin/js/jquery-3.3.1.js"></script>
    <script src="sourceadmin/js/jquery.dataTables.min.js"></script>
    <script src="sourceadmin/js/dataTables.bootstrap.min.js"></script>
    <script src="sourceadmin/js/bootstrap.min.js"></script>
    <script>
      $(document).ready(function() {
        $('#khachhang').DataTable({
          "pagingType": "full_numbers",
          "language": {
              "search": "Tìm kiếm:",
          }
        });
      });
    </script>
@endsection