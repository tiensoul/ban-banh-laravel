@extends('admin/master')
@section('contentcp')
@if(!Auth::check())
  <script>window.location = "admin/login";</script>
@endif
    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
          @if(Session::has('thongbao'))
            <div class="alert alert-success">{{ Session::get('thongbao') }}</div>
          @endif

          @if(count($errors) > 0)
            @foreach($errors->all() as $er)
                <div class="alert alert-danger">{{ $er }}</div>
            @endforeach
          @endif
        <h2>Danh sách slide</h2>
        <input type="submit" class="btn btn-success dt-add" name="themoi" value="Thêm ảnh trình chiếu">
      </div>
      <table id="sanpham" class="display table table-bordered table-hover">
       <thead>
          <tr>
             <th>ID</th>
             <th>Hình ảnh</th>
             <th>Link ảnh</th>
             <th>Thao tác</th>
          </tr>
       </thead>
       <tbody>

        @foreach($slides as $slide)
          <tr>
            <th scope="row">{{ $slide->id }}</th>
             <td><img src="source/image/slide/{{ $slide->image}}" style="width: 100px; height: 90px;" /></td>
             <td>{{ $slide->link }}</td>
             <td><a class="dt-edit" style="cursor: pointer;">Sửa</a> - <a class="dt-remove" style="cursor: pointer;">Xóa</a></td>
          </tr>
          @endforeach

       </tbody>
      </table>
    </div>

    <!-- Modal -->
    <div id="modalEdit" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form action="#"></form>
        <form action="{{ route('capnhatslide') }}" method="POST" id="form" enctype="multipart/form-data" autocomplete="off">
          @csrf
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin trình chiếu</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <input type="hidden" class="form-control disabled" name="idedit" id="idedit">
              </div>
              <div class="form-group">
                <label for="exampleInputFile">Tải lên hình ảnh</label>
                <input type="file" id="fileupload" name="fileupload">
                <p class="help-block">Tải lên hình ảnh trình chiếu</p>
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-success" name="submit" value="Cập nhật">
          </div>
          <form>
        </div>

      </div>
    </div>
    <!-- End-Modal -->

    <!-- Modal -->
    <div id="modalAdd" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <form action="#"></form>
        <form action="{{ route('themslide') }}" method="POST" id="formadd" name="formadd" enctype="multipart/form-data" autocomplete="off">
          @csrf
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin slide</h4>
          </div>
          <div class="modal-body">
              <div class="form-group">
                <label for="exampleInputFile">Tải lên hình ảnh</label>
                <input type="file" id="fileuploadadd" name="fileuploadadd">
                <p class="help-block">Tải lên slide trình chiếu</p>
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-success" name="submitadd" value="Thêm ảnh">
          </div>
          <form>
        </div>

      </div>
    </div>
    <!-- End-Modal -->

    <!-- Modal -->
    <div id="modalDelete" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form action="#"></form>
          <form action="{{ route('xoaslide') }}" method="POST" enctype="multipart/form-data">
            @csrf
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin</h4>
          </div>
          <div class="modal-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Bạn chắc chắn muốn ảnh này</label>
                <input type="hidden" class="form-control" name="idxoa" id="idxoa">
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-danger" name="xoaslide" value="Xóa ảnh này">
          </div>
          <form>
        </div>

      </div>
    </div>
    <!-- End-Modal -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="sourceadmin/js/jquery-3.3.1.js"></script>
    <script src="sourceadmin/js/jquery.dataTables.min.js"></script>
    <script src="sourceadmin/js/dataTables.bootstrap.min.js"></script>
    <script src="sourceadmin/js/bootstrap.min.js"></script>
    <script>
      $(document).ready(function() {
        $('#sanpham').DataTable({
          "pagingType": "full_numbers",
          "language": {
              "search": "Tìm kiếm:",
          }
        });
      });
    </script>
    <script>
      //Edit row buttons
      $('.dt-edit').each(function () {
        $(this).on('click', function(evt){
          $this = $(this);
          var dtRow = $this.parents('tr');
          for(var i=0; i < dtRow[0].cells.length; i++){
            $('#idedit').val(dtRow[0].cells[0].innerHTML);
          }
          $('#modalEdit').modal('show');
        });
      });

      //add
      $('.dt-add').each(function () {
        $(this).on('click', function(evt){
          $('#modalAdd').modal('show');
        });
      });

       //delete
      $('.dt-remove').each(function () {
        $(this).on('click', function(evt){
          $this = $(this);
          var dtRow = $this.parents('tr');
          for(var i=0; i < dtRow[0].cells.length; i++){
            $('#idxoa').val(dtRow[0].cells[0].innerHTML);
          }
          $('#modalDelete').modal('show');
        });
      });
    </script>
@endsection