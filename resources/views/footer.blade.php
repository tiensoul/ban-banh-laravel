<div id="footer" class="color-div">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="widget">
						<h4 class="widget-title">Hoa Doan Mobile</h4>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="widget">
						<h4 class="widget-title">Thông tin</h4>
						<div>
							<ul>
								<li><a href="{{ route('trang-chu') }}"><i class="fa fa-chevron-right"></i> Trang chủ</a></li>
								<li><a href="{{ route('loaisanpham', 1) }}"><i class="fa fa-chevron-right"></i> Sản phẩm</a></li>
								<li><a href="{{ route('gioithieu') }}"><i class="fa fa-chevron-right"></i> Giới thiệu</a></li>
								<li><a href="{{ route('gioithieu') }}"><i class="fa fa-chevron-right"></i> Liên hệ</a></li>
								<li><a href="{{ route('gioithieu') }}"><i class="fa fa-chevron-right"></i> Dịch vụ</a></li>
								<li><a href="{{ route('gioithieu') }}"><i class="fa fa-chevron-right"></i> Quảng cáo</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
				 <div class="col-sm-10">
					<div class="widget">
						<h4 class="widget-title">Liên hệ</h4>
						<div>
							<div class="contact-info">
								<i class="fa fa-map-marker"></i>
								<p>Số 123, Trường Chinh, Hà Nội</p>
								<p>Vui lòng đến cửa hàng để mua các sản phẩm với giá ưu đãi nhất</p>
							</div>
						</div>
					</div>
				  </div>
				</div>
				<div class="col-sm-3">
					<div class="widget">
						<h4 class="widget-title">Tổng đài hỗ trợ</h4>
						<form action="#" method="post">
							<p>1800.xxxx.xxxx</p>
						</form>
					</div>
				</div>
			</div> <!-- .row -->
		</div> <!-- .container -->
	</div> <!-- #footer -->
	<div class="copyright">
		<div class="container">
			<p class="pull-left">Bản quyền thuộc về (&copy;) 2019</p>
			<div class="clearfix"></div>
		</div> <!-- .container -->
	</div> <!-- .copyright -->