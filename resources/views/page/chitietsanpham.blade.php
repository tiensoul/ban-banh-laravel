@extends('master')
@section('content')
<div class="inner-header">
	<div class="container">
		<div class="space30">&nbsp;</div>
		<div class="pull-left">
			<h6 class="inner-title">Chi tiết sản phẩm</h6>
		</div>
		<div class="pull-right">
			<div class="beta-breadcrumb font-large">
				<a href="{{ route('trang-chu') }}">Home</a> / <span>Thông tin sản phẩm</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="container">
	<div id="content">
		<div class="row">
			<div class="col-sm-9">
				<div class="row">
					<div class="col-sm-4">
						<img src="source/image/product/{{ $sanpham->image }}" alt="">
					</div>
					<div class="col-sm-8">
						<div class="single-item-body">
							<p class="single-item-title" style="font-size: 20px;">{{ $sanpham->name }}</p>
							<p class="single-item-price">
								@if($sanpham->promotion_price == 0)
									<span class="flash-sale">{{ $sanpham->unit_price }}đ</span>
									@else
									<span class="flash-del">{{number_format(($sanpham->unit_price), 0, ',', '.') }}đ</span>
									<span class="flash-sale">{{number_format(($sanpham->promotion_price), 0, ',', '.') }}đ</span>
									@endif
							</p>
						</div>

						<div class="clearfix"></div>
						<div class="space20">&nbsp;</div>

						<div class="single-item-desc">
							<p><p>{{ $sanpham->description }}</p></p>
						</div>
						<div class="space20">&nbsp;</div>

						<a class="add-to-cart" href="{{ route('themgiohang', $sanpham->id) }}"><i class="fa fa-shopping-cart"></i></a>
						<div class="clearfix"></div>
					</div>
				</div>

				<div class="space40">&nbsp;</div>
				<div class="woocommerce-tabs">
					<ul class="tabs">
						<li><a href="#tab-description">Mô tả</a></li>
						<li><a href="#tab-reviews">Đánh giá (0)</a></li>
					</ul>

					<div class="panel" id="tab-description">
						<p><p>{{ $sanpham->description }}</p></p>
					</div>
					<div class="panel" id="tab-reviews">
						<p>Không có đánh giá!</p>
					</div>
				</div>
				<div class="space50">&nbsp;</div>
				<div class="beta-products-list">
					<h4>Sản phẩm liên quan</h4>
					<div class="space20">&nbsp;</div>
					<div class="space20"></div>
					<div class="row">
						@foreach($sanphamtuongtu as $sptt)
						<div class="col-sm-4" style="height: 400px;">
							<div class="single-item">
								@if($sptt->promotion_price != 0)
									<div class="ribbon-wrapper"><div class="ribbon sale1">Sale</div></div>
								@endif
								<div class="single-item-header">
									<a href="#"><img src="source/image/product/{{ $sptt->image }}" alt="" width="263px" height="263px"></a>
								</div>
								<div class="single-item-body">
									<p class="single-item-title">{{ $sptt->name }}</p>
									<p class="single-item-price">
										@if($sptt->promotion_price == 0)
												<span class="flash-sale">{{ number_format(($sptt->unit_price), 0, ',', '.') }}đ</span>
												@else
												<span class="flash-del">{{number_format(($sptt->unit_price), 0, ',', '.') }}đ</span>
												<span class="flash-sale">{{number_format(($sptt->promotion_price), 0, ',', '.') }}đ</span>
												@endif
									</p>
								</div>
								<div class="single-item-caption">
									<a class="add-to-cart pull-left" href="{{ route('themgiohang', $sptt->id) }}"><i class="fa fa-shopping-cart"></i></a>
									<a class="beta-btn primary" href="{{ route('chitietsanpham', $sptt->id) }}">Chi tiết <i class="fa fa-chevron-right"></i></a>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						@endforeach
					</div>
					<div>{{ $sanphamtuongtu->links() }}</div>
				</div> <!-- .beta-products-list -->
			</div>
			<div class="col-sm-3 aside">
				<div class="widget">
					<h3 class="widget-title">Sản phẩm đang khuyến mãi</h3>
					<div class="widget-body">
						<div class="beta-sales beta-lists">
							@foreach($spkm as $sp)
							<div class="media beta-sales-item">
							<a class="pull-left" href="{{ route('chitietsanpham', $sp->id) }}"><img src="source/image/product/{{$sp->image}}" alt="{{$sp->image}}"></a>
								<div class="media-body">
									<a href="{{ route('chitietsanpham', $sp->id) }}">{{$sp->name}}</a>
									<span class="beta-sales-price">{{number_format(($sp->promotion_price), 0, ',', '.') }}đ</span>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div> <!-- best sellers widget -->
				<div class="widget">
					<h3 class="widget-title">Sản phẩm mới</h3>
					<div class="widget-body">
						<div class="beta-sales beta-lists">
							@foreach($spm as $spmm)
							<div class="media beta-sales-item">
								<a class="pull-left" href="{{ route('chitietsanpham', $spmm->id) }}"><img src="source/image/product/{{$spmm->image}}" alt="{{$spmm->image}}"></a>
								<div class="media-body">
										<a href="{{ route('chitietsanpham', $spmm->id) }}">{{$spmm->name}}</a>
									<span class="beta-sales-price">{{number_format(($spmm->unit_price), 0, ',', '.') }}đ</span>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div> <!-- best sellers widget -->
			</div>
		</div>
	</div> <!-- #content -->
</div> <!-- .container -->
@endsection